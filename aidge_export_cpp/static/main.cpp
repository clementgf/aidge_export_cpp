
#include <iostream>
#include "dnn.hpp"
#include "inputs.h"

int main()
{
    const unsigned int nb_classes = 10;

    float results[nb_classes];
    model_forward(inputs, results);

    for (unsigned int i = 0; i < nb_classes; ++i)
    {
        std::cout << i << ": " << results[i] << std::endl;
    }

    return 0;
}