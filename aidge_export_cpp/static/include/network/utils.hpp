#ifndef __AIDGE_EXPORT_CPP_NETWORK_UTILS__
#define __AIDGE_EXPORT_CPP_NETWORK_UTILS__

/**
 * @brief   Integer clamping
 * @param[in]  v   Value to be clamped
 * @param[in]  lo  Saturating lower bound
 * @param[in]  hi  Saturating higher bound
 * @returns         Value clamped between lo and hi
 * 
 */
__attribute__((always_inline)) static inline
int clamp (int v, int lo, int hi) 
{
    if(v < lo) {
        return lo;
    }
    else if(v > hi) {
        return hi;
    }
    else {
        return v;
    }
}

/**
 * @brief   Maximum of two integer values
 */
__attribute__((always_inline)) static inline
int max (int lhs, int rhs) 
{
    return (lhs >= rhs) ? lhs : rhs;
}

/**
 * @brief   Minimum of two integer values
 */
__attribute__((always_inline)) static inline
int min (int lhs, int rhs) 
{
    return (lhs <= rhs) ? lhs : rhs;
}

#endif  // __AIDGE_EXPORT_CPP_NETWORK_UTILS__
