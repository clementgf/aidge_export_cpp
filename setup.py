#!/usr/bin/env python3
""" Aidge Export CPP
"""

DOCLINES = (__doc__ or '').split("\n")

import sys

# Python supported version checks
if sys.version_info[:2] < (3, 7):
    raise RuntimeError("Python version >= 3.7 required.")


CLASSIFIERS = """\
Development Status :: 2 - Pre-Alpha
"""

import pathlib

from setuptools import setup
from setuptools import find_packages

def get_project_version() -> str:
    aidge_root = pathlib.Path().absolute()
    version = open(aidge_root / "version.txt", "r").read().strip()
    return version


if __name__ == '__main__':

    setup(
        name="aidge_export_cpp",
        version=get_project_version(),
        python_requires='>=3.7',
        description=DOCLINES[0],
        long_description_content_type="text/markdown",
        long_description="\n".join(DOCLINES[2:]),
        classifiers=[c for c in CLASSIFIERS.split('\n') if c],
        platforms=["Linux"],
        packages=find_packages(where="."),
        include_package_data=True,
    )
